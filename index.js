const express = require('express');
const app = express();
const morgan = require('morgan');

app.use(morgan('short'));
app.use(express.static('build'));

app.get('/test', (req, res) => {
    res.send({ status: 1 });
})

app.listen(() => {
    console.log('listening...');
});